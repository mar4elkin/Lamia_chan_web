# Lamia chan

## dependence
### python

django version:2.2

python: 3.6+

```
pip install django 
pip install django-cors-headers
pip install djangorestframework
pip install Pillow
```

### javascript 

nodejs version: v12.13.1

npm version: 6.12.1

vue/cli: 4.1.1

```
sudo apt install nodejs
sudo apt install npm
npm install vue   
sudo npm install -g @vue/cli
```


#### startproject

```
npm run serve --prefix frontend/Lamia_chan & python backend/manage.py runserver
```


#### TODO

* Разобраться с api сделать его более удобным
* Сделать выводы на frontend через api
* Разобраться с загрузкой картинок
